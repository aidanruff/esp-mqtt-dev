<html>
<head><title>Esp8266 web server</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="main">
<h1>Hackitt & Bodgitt Designs</h1>
<p>
ESP8266 MQTT device Configuration<br> Aidan Ruff (aidan@ruffs.org) and Peter Scargill (peter@scargill.org)<br>
(Aka H & B Designs)
<ul>
<li>Setup the WiFi access and MQTT Server details <a href="/wifi"> here</a><br></li>
<li>Control the <a href="led.tpl">Relay and LED</a><br></li>
<li>You can download the raw binary image of the ESP module <a href="flash.bin">Here</a></li>
</ul>
</p>

</p>
</div>
</body></html>
